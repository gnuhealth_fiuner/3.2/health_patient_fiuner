#-*- coding: utf-8 -*-

from datetime import date, datetime, timedelta
from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
import calendar

__all__ = ['PatientData']

class PatientData(metaclass = PoolMeta):    
    'Datos Paciente'
    __name__ = 'gnuhealth.patient'

    edad = fields.Function(fields.Char('Edad'),'get_patient_age', searcher='search_edad')

#    def get_patient_age(self, name):
#        res = self.name.age
#        if res:  
#            if (res.find('y')==1 or res.find('y')==2):
#                pos_y = res.find('y')
#                pos_m = res.find('m')
#                pos_d = res.find('d')
#                res = float(res[:pos_y]) + (float(res[pos_y+1:pos_m])/12.0) + (float(res[pos_m+1:pos_d])/365.0)
#                return str(res)[:7]
#        return '0'
    
    @classmethod
    def search_edad(cls, name, clause):
        transaction = Transaction()
        connection = transaction.connection
        cursor = connection.cursor()
        _, operator,operand1  = clause
        
        operand1 = int(operand1.replace('%',''))        
        curr_year = date.today().year
        
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')
        
        patient = Patient.__table__()
        party = Party.__table__()
        result1 = []
        result2 = []

        if operator in {'ilike','='}:
            operand2_first_day = date.today().replace(year=curr_year-operand1-1)
            operand2_last_day = date.today().replace(year=curr_year-operand1)
            query1 = (party.select(party.id,
                    where=((party.dob >=operand2_first_day)
                            &(party.dob<=operand2_last_day)
                            &(party.is_patient==True))))
            cursor.execute(*query1)
            result1 = cursor.fetchall()
        else:
            operand2 = date.today().replace(year=curr_year-operand1-1)
            Operator = fields.SQL_OPERATORS[clause[1]]
            print('\n\n\n\n\n\n')
            print(Operator)
            print(operand2)
            print(Operator(party.dob,operand2))
            print('\n\n\n\n\n\n')
            query1 = (party.select(party.id,
                    where=((Operator(operand2,party.dob))
                            &(party.is_patient==True))))
            cursor.execute(*query1)
            result1 = cursor.fetchall()        
        
        if result1:
            query2 = (patient.select(patient.id,
                where=(patient.name.in_(result1))))
            cursor.execute(*query2)
            result2 = cursor.fetchall()
            return [('id','in',result2)]
        return [('id','=',0)]

    sexo = fields.Function(fields.Selection([('m', 'Masculino'), ('f', 'Femenino')], 'Sexo'),'get_sex')

    def get_sex(self, name):
        return self.gender

    dob = fields.Function(fields.Date('DoB'), 'get_patient_dob')

    def get_patient_dob(self, name):
        return self.name.dob
        
    barrio = fields.Function(fields.Char('Barrio'), 'get_patient_operational_sector', searcher='search_barrio')

    def get_patient_operational_sector(self, name):
        if self.name.du:
            if self.name.du.operational_sector:            
                return  self.name.du.operational_sector.name
        else:
            return 'Sin Dato'

    @classmethod
    def search_barrio(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('name.du.operational_sector.name', clause[1], value))
        return res

    hc = fields.Char('HC')

    cronico = fields.Selection([
        (None, ''),
        ('Si', 'Si'),
        ('No', 'No'),
        ],'Cronico',help = 'Paciente con medicacion de cronico'
        , sort=False)
